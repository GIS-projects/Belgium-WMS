
# WMS List Belgium
**A list of WMS services with data for Belgium**

The entire content of this repository is stored in this README.md document. All editing should be done on this file. The only other file is the LICENSE file.

More information about WMS Services kan be found on [Wikipedia](https://en.wikipedia.org/wiki/Web_Map_Service).

More detailed information about all listed services can be found on [wms.michelstuyts.be](https://wms.michelstuyts.be).

An xml file to import all these services into [QGIS](https://qgis.org) can be downloaded from https://wms.michelstuyts.be/qgis.php?lang=en.

## Belgium

* [Aeronautical Obstacle Evaluation Map for Belgium - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/dgta_dglv/service) - [GetCapabilities](https://wms.ngi.be/inspire/dgta_dglv/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [Cadastral Layers Belgium - SPF Finances](https://ccff02.minfin.fgov.be/geoservices/arcgis/services/WMS/Cadastral_Layers/MapServer/WMSServer) - [GetCapabilities](https://ccff02.minfin.fgov.be/geoservices/arcgis/services/WMS/Cadastral_Layers/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Cadastral parcels Belgium - FPS Finances - General Administration of Patrimonial Documentation (GAPD)](https://ccff02.minfin.fgov.be/geoservices/arcgis/services/INSPIRE/CP/MapServer/WmsServer) - [GetCapabilities](https://ccff02.minfin.fgov.be/geoservices/arcgis/services/INSPIRE/CP/MapServer/WmsServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CartoWeb.be NGI-IGN - National Geographic Institute - Spatial Data Infrastructure](https://cartoweb.wms.ngi.be/service) - [GetCapabilities](https://cartoweb.wms.ngi.be/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [Digital surface model Meuse, Vesdre and Demer - National Geographic Institute](https://wms.ngi.be/inspire/flood_dsm/service) - [GetCapabilities](https://wms.ngi.be/inspire/flood_dsm/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [eTOD database - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/etod/service) - [GetCapabilities](https://wms.ngi.be/inspire/etod/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [Flanders Marine Institute (VLIZ) - Geoserver WMS Service - VLIZ](https://geo.vliz.be/geoserver/ows) - [GetCapabilities](https://geo.vliz.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [Flanders Marine Institute (VLIZ) - Geoserver WMS Service - VLIZ](https://geo.vliz.be/geoserver/Scheldemonitor/ows) - [GetCapabilities](https://geo.vliz.be/geoserver/Scheldemonitor/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [Geo.be - Territorial divisions - National Geographic Institute](https://data.geo.be/ws/territorialdivisions/wms) - [GetCapabilities](https://data.geo.be/ws/territorialdivisions/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Web Map Service - Biodiversity and Ecosystems Data and Information Centre (BEDIC)](https://spatial.naturalsciences.be/geoserver/imsp/ows) - [GetCapabilities](https://spatial.naturalsciences.be/geoserver/imsp/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Web Map Service - Royal Observatory of Belgium](https://inspire.seismology.be/geoserver/ows) - [GetCapabilities](https://inspire.seismology.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Web Map Service - Biodiversity and Ecosystems Data and Information Centre (BEDIC)](https://spatial.naturalsciences.be/geoserver/ows) - [GetCapabilities](https://spatial.naturalsciences.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Web Map Service - geo.bipt-data.be](https://geo.bipt-data.be/geoserver/wms) - [GetCapabilities](https://geo.bipt-data.be/geoserver/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Web Map Service - Biodiversity and Ecosystems Data and Information Centre (BEDIC)](https://spatial.naturalsciences.be/geoserver/od_nature/ows) - [GetCapabilities](https://spatial.naturalsciences.be/geoserver/od_nature/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Web Map Service - Biodiversity and Ecosystems Data and Information Centre (BEDIC)](https://spatial.naturalsciences.be/geoserver/MSFD/ows) - [GetCapabilities](https://spatial.naturalsciences.be/geoserver/MSFD/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Web Map Service - Biodiversity and Ecosystems Data and Information Centre (BEDIC)](https://spatial.naturalsciences.be/geoserver/ge/ows) - [GetCapabilities](https://spatial.naturalsciences.be/geoserver/ge/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Web Map Service - Biodiversity and Ecosystems Data and Information Centre (BEDIC)](https://spatial.naturalsciences.be/geoserver/idod/ows) - [GetCapabilities](https://spatial.naturalsciences.be/geoserver/idod/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [Govroam access points localization - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/govroam/service) - [GetCapabilities](https://wms.ngi.be/inspire/govroam/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE View service - Buildings - National Geographic Institute](https://data.geo.be/ws/building/wms) - [GetCapabilities](https://data.geo.be/ws/building/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE View service - Contour lines - National Geographic Institute](https://data.geo.be/ws/elevation/wms) - [GetCapabilities](https://data.geo.be/ws/elevation/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE View service - CORINE Land Cover-2018-Belgium - National Geographic Institute](https://data.geo.be/ws/landcover/wms) - [GetCapabilities](https://data.geo.be/ws/landcover/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE View service - Electricity network - National Geographic Institute](https://data.geo.be/ws/utility/wms) - [GetCapabilities](https://data.geo.be/ws/utility/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE View Service - FPS Economy - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/dgstatistics/service) - [GetCapabilities](https://wms.ngi.be/inspire/dgstatistics/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE View Service - FPS Economy - National Geographic Institute](https://data.geo.be/ws/dgstatistics/wms) - [GetCapabilities](https://data.geo.be/ws/dgstatistics/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [IRCEL - CELINE - Web Map Service - Belgian Interregional Environment Agency](https://geo.irceline.be/aqd/wms) - [GetCapabilities](https://geo.irceline.be/aqd/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [IRCEL - CELINE - Web Map Service - Belgian Interregional Environment Agency](https://geo.irceline.be/aqd/air_quality_assessment_zones/wms) - [GetCapabilities](https://geo.irceline.be/aqd/air_quality_assessment_zones/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [IRCEL - CELINE - Web Map Service - Belgian Interregional Environment Agency](https://geo.irceline.be/wms) - [GetCapabilities](https://geo.irceline.be/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [IRCEL - CELINE - Web Map Service - Belgian Interregional Environment Agency](https://geo.irceline.be/aqd/ows) - [GetCapabilities](https://geo.irceline.be/aqd/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [IRCEL - CELINE - Web Map Service - Belgian Interregional Environment Agency](https://geo.irceline.be/rio/ows) - [GetCapabilities](https://geo.irceline.be/rio/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [IRCEL - CELINE - Web Map Service - Belgian Interregional Environment Agency](https://geo.irceline.be/rioifdm/wms) - [GetCapabilities](https://geo.irceline.be/rioifdm/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [IRCEL - CELINE - Web Map Service - Belgian Interregional Environment Agency](https://geo.irceline.be/rioifdm/ows) - [GetCapabilities](https://geo.irceline.be/rioifdm/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [IRCEL - CELINE - Web Map Service - Belgian Interregional Environment Agency](https://geo.irceline.be/realtime/ows) - [GetCapabilities](https://geo.irceline.be/realtime/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [IRCEL - CELINE - Web Map Service - Belgian Interregional Environment Agency](https://geo.irceline.be/annual/ows) - [GetCapabilities](https://geo.irceline.be/annual/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [KMI-IRM - Web Map Service - Royal Meteorological Institute of Belgium](https://opendata.meteo.be/service/aws/ows) - [GetCapabilities](https://opendata.meteo.be/service/aws/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [KMI-IRM - Web Map Service - Royal Meteorological Institute of Belgium](https://opendata.meteo.be/service/lidar/ows) - [GetCapabilities](https://opendata.meteo.be/service/lidar/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [KMI-IRM - Web Map Service - Royal Meteorological Institute of Belgium](https://opendata.meteo.be/service/synop/ows) - [GetCapabilities](https://opendata.meteo.be/service/synop/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [KMI-IRM - Web Map Service - Royal Meteorological Institute of Belgium](https://opendata.meteo.be/service/alaro/ows) - [GetCapabilities](https://opendata.meteo.be/service/alaro/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [MapIndex - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/mapindex/service) - [GetCapabilities](https://wms.ngi.be/inspire/mapindex/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [Minfin - Administrative units - FPS Finances - General Administration of Patrimonial Documentation (GAPD)](https://ccff02.minfin.fgov.be/geoservices/arcgis/services/INSPIRE/AU/Mapserver/WMSServer) - [GetCapabilities](https://ccff02.minfin.fgov.be/geoservices/arcgis/services/INSPIRE/AU/Mapserver/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Municipalities - eservices.minfin.fgov.be](https://eservices.minfin.fgov.be/arcgis/services/R3C/Municipalities/MapServer/WMSServer) - [GetCapabilities](https://eservices.minfin.fgov.be/arcgis/services/R3C/Municipalities/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [National wastewater-based epidemiological surveillance of SARS-CoV-2 (WMS) - National Geographic Institute](https://data.geo.be/ws/sciensano/wms) - [GetCapabilities](https://data.geo.be/ws/sciensano/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [NGI INSPIRE View Service - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/ngi_inspire_view_service/service) - [GetCapabilities](https://wms.ngi.be/inspire/ngi_inspire_view_service/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [NGI INSPIRE View Service - National Geographic Institute](https://data.geo.be/ws/inspire_annex1/wms) - [GetCapabilities](https://data.geo.be/ws/inspire_annex1/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Ortho Floods 2021 - National Geographic Institute](https://wms.ngi.be/inspire/flood_ortho/service) - [GetCapabilities](https://wms.ngi.be/inspire/flood_ortho/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthophoto WMS - National Geographic Institute](https://wms.ngi.be/inspire/ortho/service) - [GetCapabilities](https://wms.ngi.be/inspire/ortho/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [ROB Inspire GeoServer - Royal Observatory of Belgium](https://inspire.seismology.be/geoserver/inspire/wms) - [GetCapabilities](https://inspire.seismology.be/geoserver/inspire/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [SUVIM station network WMS - National Geographic Institute](https://wms.ngi.be/inspire/aeronomie/service) - [GetCapabilities](https://wms.ngi.be/inspire/aeronomie/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [TopoMaps - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/topomaps/service) - [GetCapabilities](https://wms.ngi.be/inspire/topomaps/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [View service - altimetry - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/dtm/service) - [GetCapabilities](https://wms.ngi.be/inspire/dtm/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [View service - Climate indicators for Belgium - National Geographic Institute](https://data.geo.be/ws/bcc/wms) - [GetCapabilities](https://data.geo.be/ws/bcc/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [View service - CORINE Land Cover - Belgium - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/clc/service) - [GetCapabilities](https://wms.ngi.be/inspire/clc/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [View service - LandUse-Particular Zone - National Geographic Institute](https://data.geo.be/ws/landuse/wms) - [GetCapabilities](https://data.geo.be/ws/landuse/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [View service - Marine Strategy Framework Directive - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/rbins/service) - [GetCapabilities](https://wms.ngi.be/inspire/rbins/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [View service - Marine Strategy Framework Directive - National Geographic Institute](https://data.geo.be/ws/rbins/wms) - [GetCapabilities](https://data.geo.be/ws/rbins/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [View service - Territorial divisions - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/admin/service) - [GetCapabilities](https://wms.ngi.be/inspire/admin/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [View service -Top10Vector - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/ngi_top10v_view_service/service) - [GetCapabilities](https://wms.ngi.be/inspire/ngi_top10v_view_service/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [View service for BeStAddress - National Geographic Institute](https://data.geo.be/ws/best/wms) - [GetCapabilities](https://data.geo.be/ws/best/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [View service for CORINE High Resolution Layers - National Geographic Institute - Spatial Data Infrastructure](https://wms.ngi.be/inspire/hrl/service) - [GetCapabilities](https://wms.ngi.be/inspire/hrl/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [View service for dataset CORINE Local Components - National Geographic Institute](https://wms.ngi.be/inspire/copernicus/service) - [GetCapabilities](https://wms.ngi.be/inspire/copernicus/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geoserver.aeronomie.be](https://geoserver.aeronomie.be/geoserver/Communication/ows) - [GetCapabilities](https://geoserver.aeronomie.be/geoserver/Communication/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS bouwmeester maitre architecte Brussel/Bruxelles - Speculoos SPRL](http://bmawms.specgis.be/service) - [GetCapabilities](http://bmawms.specgis.be/service?REQUEST=GetCapabilities&SERVICE=WMS)



## Brussels

* [Air, climat, énerie // Lucht, klimaat, energie - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://ows.environnement.brussels/air) - [GetCapabilities](https://ows.environnement.brussels/air?REQUEST=GetCapabilities&SERVICE=WMS)

* [Bruenvi Bruenvi- WMS DATA NO INSPIRE - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://wms.environnement.brussels/be_wms) - [GetCapabilities](https://wms.environnement.brussels/be_wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Bruenvi Bruenvi- WMS DATA NO INSPIRE - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://wms.environnement.brussels/lb_wms) - [GetCapabilities](https://wms.environnement.brussels/lb_wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Bruit // Geluid - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://ows.environnement.brussels/noise) - [GetCapabilities](https://ows.environnement.brussels/noise?REQUEST=GetCapabilities&SERVICE=WMS)

* [CIRB WMS - Paradigm](https://geoservices-urbis.irisnet.be/geoserver/ows) - [GetCapabilities](https://geoservices-urbis.irisnet.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [CIRB WMS - Paradigm](https://geoservices-urbis.irisnet.be/geoserver/Urbis/ows) - [GetCapabilities](https://geoservices-urbis.irisnet.be/geoserver/Urbis/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [CIRB WMS - Paradigm](https://geoservices-urbis.irisnet.be/geoserver/UrbisAdm/ows) - [GetCapabilities](https://geoservices-urbis.irisnet.be/geoserver/UrbisAdm/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [Eau // Water - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://ows.environnement.brussels/water) - [GetCapabilities](https://ows.environnement.brussels/water?REQUEST=GetCapabilities&SERVICE=WMS)

* [Espaces verts et biodiversité // Groene ruimten en biodiversiteit - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://ows.environnement.brussels/green) - [GetCapabilities](https://ows.environnement.brussels/green?REQUEST=GetCapabilities&SERVICE=WMS)

* [Géologie et hydrogéologie // Geologie en hydrogeologie - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://ows.environnement.brussels/geology) - [GetCapabilities](https://ows.environnement.brussels/geology?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Web Map Service - urban.brussels](https://gis.urban.brussels/geoserver/ows) - [GetCapabilities](https://gis.urban.brussels/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [Public Space Brussels - Brussel Mobiliteit](https://data-mobility.irisnet.be/geoserver/bm_public_space/ows) - [GetCapabilities](https://data-mobility.irisnet.be/geoserver/bm_public_space/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [Sol et sous-sol // Bodem en ondergrond - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://ows.environnement.brussels/soil) - [GetCapabilities](https://ows.environnement.brussels/soil?REQUEST=GetCapabilities&SERVICE=WMS)

* [Ville en transition // Stad in transitie - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://ows.environnement.brussels/transition) - [GetCapabilities](https://ows.environnement.brussels/transition?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Brussel Mobiliteit](https://data.mobility.brussels/geoserver/bm_public_transport/wms) - [GetCapabilities](https://data.mobility.brussels/geoserver/bm_public_transport/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Brussel Mobiliteit](https://data.mobility.brussels/geoserver/bm_network/wms) - [GetCapabilities](https://data.mobility.brussels/geoserver/bm_network/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Brussel Mobiliteit](https://data.mobility.brussels/geoserver/bm_equipment/wms) - [GetCapabilities](https://data.mobility.brussels/geoserver/bm_equipment/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Brussel Mobiliteit](https://data.mobility.brussels/geoserver/bm_security/wms) - [GetCapabilities](https://data.mobility.brussels/geoserver/bm_security/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Brussel Mobiliteit](https://data.mobility.brussels/geoserver/bm_bike/wms) - [GetCapabilities](https://data.mobility.brussels/geoserver/bm_bike/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Web map service for visualisation of BruGIS, urban - urban.brussels](https://gis.urban.brussels/geoserver/URBAN_DCH_IBH/ows) - [GetCapabilities](https://gis.urban.brussels/geoserver/URBAN_DCH_IBH/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Web map service for visualisation of BruGIS, urban - urban.brussels](https://gis.urban.brussels/geoserver/RCMS/ows) - [GetCapabilities](https://gis.urban.brussels/geoserver/RCMS/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Web map service for visualisation of BruGIS, urban - urban.brussels](https://gis.urban.brussels/geoserver/URBAN_DCH_AH/ows) - [GetCapabilities](https://gis.urban.brussels/geoserver/URBAN_DCH_AH/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Web map service for visualisation of BruGIS, urban - urban.brussels](https://gis.urban.brussels/geoserver/URBAN_DUR/ows) - [GetCapabilities](https://gis.urban.brussels/geoserver/URBAN_DUR/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Web map service for visualisation of BruGIS, urban - urban.brussels](https://gis.urban.brussels/geoserver/URBAN_DUP/ows) - [GetCapabilities](https://gis.urban.brussels/geoserver/URBAN_DUP/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Brussel Mobiliteit - Bruxelles Mobilite / Brussel Mobiliteit](https://data-mobility.irisnet.be/inspire/capabilities/nl/wms) - [GetCapabilities](https://data-mobility.irisnet.be/inspire/capabilities/nl/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Bruxelles Mobilite - Bruxelles Mobilite - Brussel Mobiliteit](https://data-mobility.irisnet.be/inspire/capabilities/fr/wms) - [GetCapabilities](https://data-mobility.irisnet.be/inspire/capabilities/fr/wms?REQUEST=GetCapabilities&SERVICE=WMS)



## Flanders

* [3D GRB - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/3DGRB/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/3DGRB/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Administratieve eenheden - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Administratieve_Eenheden/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/Administratieve_Eenheden/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Adressen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/ad/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/ad/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [advieskaart - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/advieskaart/MapServer/WMSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/advieskaart/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [afstroomgebieden_watertoets - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/afstroomgebieden_watertoets/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/afstroomgebieden_watertoets/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ANB_zones - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/ANB/zones/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/ANB/zones/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [archief_Overstromingsgevoelige_gebieden_2014 - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/archief/Overstromingsgevoelige_gebieden_2014/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/archief/Overstromingsgevoelige_gebieden_2014/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [archief_Overstromingsgevoelige_gebieden_2017 - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/archief/Overstromingsgevoelige_gebieden_2017/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/archief/Overstromingsgevoelige_gebieden_2017/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Atlas der buurtwegen - Provincie West-Vlaanderen](https://www.geoloket.be/gwserver/services/WMS/AGS_Atlas_bw_plan_WMS/MapServer/WMSServer) - [GetCapabilities](https://www.geoloket.be/gwserver/services/WMS/AGS_Atlas_bw_plan_WMS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Atlas der Waterlopen Oost-Vlaanderen - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/MIL/atlaswaterlopen/Mapserver/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/MIL/atlaswaterlopen/Mapserver/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Basisdata Oost-Vlaanderen - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/BAS/Basisdata/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/BAS/Basisdata/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Basislagen en Luchtfoto's Gent - Gent](https://geo.gent.be/geoserver/SG-E-BasislagenLuchtfotos/wms) - [GetCapabilities](https://geo.gent.be/geoserver/SG-E-BasislagenLuchtfotos/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Bodemgebruik - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Bodemgebruik/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/Bodemgebruik/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Bouwen en Wonen Gent - Gent](https://geo.gent.be/geoserver/SG-E-BouwenWonen/wms) - [GetCapabilities](https://geo.gent.be/geoserver/SG-E-BouwenWonen/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Bovenlokaal Functioneel Fietsroutenetwerk - Provincie West-Vlaanderen](https://www.geoloket.be/gwserver/services/Mobiliteit/AGS_fietsroutenetwerk/MapServer/WMSServer) - [GetCapabilities](https://www.geoloket.be/gwserver/services/Mobiliteit/AGS_fietsroutenetwerk/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [captatie_captatieverboden - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/captatie/captatieverboden/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/captatie/captatieverboden/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Cultuur, Sport en Vrije tijd Gent - Gent](https://geo.gent.be/geoserver/SG-E-CultuurSportVrijetijd/wms) - [GetCapabilities](https://geo.gent.be/geoserver/SG-E-CultuurSportVrijetijd/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [dataservices_AtlasBuurtwegen_wijzigingen - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/AtlasBuurtwegen_wijzigingen/MapServer/WMSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/AtlasBuurtwegen_wijzigingen/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [dataservices_BFF - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/BFF/MapServer/WMSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/BFF/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [dataservices_FSW - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/FSW/MapServer/WMSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/FSW/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [dataservices_RecreatieveNetwerken - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/RecreatieveNetwerken/MapServer/WMSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/RecreatieveNetwerken/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [dataservices_TrageWegen - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/TrageWegen/MapServer/WMSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/TrageWegen/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Departement Cultuur, Jeugd en Media - data.uitwisselingsplatform.be](https://data.uitwisselingsplatform.be/be.dcjm.infrastructuur/datamerge-cji/cjmgeopunt/ows) - [GetCapabilities](https://data.uitwisselingsplatform.be/be.dcjm.infrastructuur/datamerge-cji/cjmgeopunt/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [digitale atlas - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/digitale_atlas/MapServer/WMSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/digitale_atlas/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [DMOW Public WMS - Vlaamse overheid, Departement Mobiliteit en Openbare Werken](https://geoserver.gis.cloud.mow.vlaanderen.be/geoserver/ows) - [GetCapabilities](https://geoserver.gis.cloud.mow.vlaanderen.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [DMOW Public WMS - Vlaamse overheid, Departement Mobiliteit en Openbare Werken](https://geoserver.gis.cloud.mow.vlaanderen.be/geoserver/wms) - [GetCapabilities](https://geoserver.gis.cloud.mow.vlaanderen.be/geoserver/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Ecotoopkwetsbaarheid - gisservices.inbo.be](https://gisservices.inbo.be/arcgis/services/Ecotoopkwetsbaarheid/MapServer/WMSServer) - [GetCapabilities](https://gisservices.inbo.be/arcgis/services/Ecotoopkwetsbaarheid/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EMODnet Biology - Flanders Marine Institute (VLIZ)](https://geo.vliz.be/geoserver/Emodnet/ows) - [GetCapabilities](https://geo.vliz.be/geoserver/Emodnet/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [Energie Sparen - Programma Earth Observation Data Science (EODaS), Agentschap Informatie Vlaanderen](https://apps.energiesparen.be/proxy/remote-sensing/geoserver/VEA/wms) - [GetCapabilities](https://apps.energiesparen.be/proxy/remote-sensing/geoserver/VEA/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [FWS_Fietssnelwegen - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/FSW/Fietssnelwegen/MapServer/WMSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/FSW/Fietssnelwegen/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Gebiedsbeheer/gebieden waar beperkingen gelden/gereguleerde gebieden en rapportage-eenheden - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/am/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/am/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Gebouwen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/bu/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/bu/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [GEODATA_Geodata_Oost_Vlaanderen - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/GEODATA/Geodata_Oost_Vlaanderen/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/GEODATA/Geodata_Oost_Vlaanderen/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Gent - Gent](https://geo.gent.be/geoserver/ows) - [GetCapabilities](https://geo.gent.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Web Map Service - geo.onroerenderfgoed.be](https://geo.onroerenderfgoed.be/geoserver/ows) - [GetCapabilities](https://geo.onroerenderfgoed.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Web Map Service - geo.onroerenderfgoed.be](https://geo.onroerenderfgoed.be/geoserver/wms) - [GetCapabilities](https://geo.onroerenderfgoed.be/geoserver/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [GeoServer Web Map Service - tool.smartgeotherm.be](https://tool.smartgeotherm.be/wtcb/geoserver/WTCB/ows) - [GetCapabilities](https://tool.smartgeotherm.be/wtcb/geoserver/WTCB/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [GIPOD Publieke Informatie - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/GIPOD/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/GIPOD/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Hoogte - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/el/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/el/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Hydrografie - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/hy/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/hy/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Indicatieve normenkaart Oost-Vlaanderen - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/MIL/Indicatieve_Normenkaart/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/MIL/Indicatieve_Normenkaart/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Indicatoren Vlaams Verkeerscentrum - indicatoren.verkeerscentrum.be](http://indicatoren.verkeerscentrum.be/geoserver/ows) - [GetCapabilities](http://indicatoren.verkeerscentrum.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [informatieplicht_overstromingsgevoelige_gebieden_fluviaal - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/informatieplicht/overstromingsgevoelige_gebieden_fluviaal/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/informatieplicht/overstromingsgevoelige_gebieden_fluviaal/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [informatieplicht_overstromingsgevoelige_gebieden_pluviaal - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/informatieplicht/overstromingsgevoelige_gebieden_pluviaal/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/informatieplicht/overstromingsgevoelige_gebieden_pluviaal/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [informatieplicht_overstromingsgevoelige_gebieden_vanuit_de_zee - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/informatieplicht/overstromingsgevoelige_gebieden_vanuit_de_zee/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/informatieplicht/overstromingsgevoelige_gebieden_vanuit_de_zee/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Inspiratiekaart Renovatiebeleid - geo.production.ikrb.agifly.cloud](https://geo.production.ikrb.agifly.cloud/wms) - [GetCapabilities](https://geo.production.ikrb.agifly.cloud/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Inspiratiekaart Renovatiebeleid - geo.production.ikrb.agifly.cloud](https://geo.production.ikrb.agifly.cloud/geoserver/ows) - [GetCapabilities](https://geo.production.ikrb.agifly.cloud/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE - Annex I - aanduidingsobjecten- Agentschap Onroerend Erfgoed - Onroerend Erfgoed](https://haleconnect.com/ows/services/org.989.ebcff89d-1bed-48d3-b915-6158d060c53d_wms) - [GetCapabilities](https://haleconnect.com/ows/services/org.989.ebcff89d-1bed-48d3-b915-6158d060c53d_wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE - Annex I - duinendecreet - Agentschap Natuur en Bos - Agentschap voor Natuur en Bos](https://haleconnect.com/ows/services/org.989.f7f7079d-9840-4608-bdd7-56174236bef6_wms) - [GetCapabilities](https://haleconnect.com/ows/services/org.989.f7f7079d-9840-4608-bdd7-56174236bef6_wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE - Annex I - Natura2000 - Agentschap Natuur en Bos - Agentschap voor Natuur en Bos](https://haleconnect.com/ows/services/org.989.adbecc04-e6d0-48ad-ba11-4b3f7781f090_wms) - [GetCapabilities](https://haleconnect.com/ows/services/org.989.adbecc04-e6d0-48ad-ba11-4b3f7781f090_wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE - Annex I - Ramsar-gebieden - Agentschap Natuur en Bos - Agentschap Natuur en Bos](https://haleconnect.com/ows/services/org.989.a32670c8-8dd5-43a8-af82-edba5392498a_wms) - [GetCapabilities](https://haleconnect.com/ows/services/org.989.a32670c8-8dd5-43a8-af82-edba5392498a_wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE Raadpleegdienst Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver/wms) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE Raadpleegdienst Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver/gw_beleid/wms) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver/gw_beleid/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Kadastrale gegevens (Historiek) Oost-Vlaanderen - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/referentie/kadhistoriek/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/referentie/kadhistoriek/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [klimaatportaal_achtergrond_KP - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/klimaatportaal/achtergrond_KP/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/klimaatportaal/achtergrond_KP/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Kunstwerken - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/Kunstwerken/MapServer/WMSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/Kunstwerken/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Landgebruik - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Landgebruik/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/Landgebruik/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Landgebruik - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/lu/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/lu/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [LiDAR pointcloud tiles and raster data: DTM, DSM - Digitaal Vlaanderen](https://remotesensing.vlaanderen.be/services/openlidar/wms) - [GetCapabilities](https://remotesensing.vlaanderen.be/services/openlidar/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [meetpunten - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/meetpunten/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/meetpunten/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [meetpunten_MOW - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/meetpunten_MOW/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/meetpunten_MOW/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Mercator Publieke View Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://www.mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/wms) - [GetCapabilities](https://www.mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Mercator Publieke View Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/ni/wms) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/ni/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Mercator Publieke View Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/us/wms) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/us/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Mercator Publieke View Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/hh/wms) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/hh/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Mercator Publieke View Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/lu/wms) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/lu/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Mercator Publieke View Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/hb/wms) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/hb/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Mercator Publieke View Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/am/wms) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/am/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Mercator Publieke View Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/pf/wms) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/pf/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Mercator Publieke View Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/er/wms) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/er/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Mercator Publieke View Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/so/wms) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/so/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Mercator Publieke View Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/ps/wms) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/ps/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Mercator Publieke View Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://www.mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/ows) - [GetCapabilities](https://www.mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [Milieubewakingsvoorzieningen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/ef/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/ef/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Neutrale_achtergrondkaart - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/Neutrale_achtergrondkaart/MapServer/WMSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/Neutrale_achtergrondkaart/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Neutrale_achtergrond_intern - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/waterinfo/Neutrale_achtergrond_intern/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/waterinfo/Neutrale_achtergrond_intern/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Nutsdiensten en overheidsdiensten - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/us/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/us/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Onroerend Erfgoed GeoPortaal MapProxy - Flanders Heritage Agency](https://geo.onroerenderfgoed.be/mapproxy/service) - [GetCapabilities](https://geo.onroerenderfgoed.be/mapproxy/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [Open Data Agentschap Wegen en Verkeer - Agentschap Wegen en Verkeer](https://opendata.apps.mow.vlaanderen.be/opendata-geoserver/awv/wms) - [GetCapabilities](https://opendata.apps.mow.vlaanderen.be/opendata-geoserver/awv/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Open Data Agentschap Wegen en Verkeer - Agentschap Wegen en Verkeer](https://opendata.apps.mow.vlaanderen.be/opendata-geoserver/ows) - [GetCapabilities](https://opendata.apps.mow.vlaanderen.be/opendata-geoserver/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthobeeldvorming - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/oi/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/oi/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthofoto archief Oost-Vlaanderen - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/referentie/Orthofoto_archief/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/referentie/Orthofoto_archief/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthofotomozaïek, grootschalig, winteropnamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OGW/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/OGW/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthofotomozaïek, kleinschalig, zomeropnamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OKZ/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/OKZ/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthofotomozaïek, middenschalig, winteropnamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OMW/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/OMW/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthofotomozaïek, middenschalig, winteropnamen, kleur, meest recent, Vlaanderen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OMWRGBMRVL/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/OMWRGBMRVL/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthofotomozaïek, middenschalig, zomeropnamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OMZ/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/OMZ/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthofotowerkbestand - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/ofw/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/ofw/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Overstromingskaarten - Vlaamse Milieu Maatschappij](https://geoservice.waterinfo.be/Overstromingsgevaarkaarten-FLUVIAAL/ows) - [GetCapabilities](https://geoservice.waterinfo.be/Overstromingsgevaarkaarten-FLUVIAAL/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [Overstromingskaarten - Vlaamse Milieu Maatschappij](https://geoservice.waterinfo.be/Overstromingsrisicokaarten-PLUVIAAL/ows) - [GetCapabilities](https://geoservice.waterinfo.be/Overstromingsrisicokaarten-PLUVIAAL/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [Overstromingskaarten - Vlaamse Milieu Maatschappij](https://geoservice.waterinfo.be/OGRK/wms) - [GetCapabilities](https://geoservice.waterinfo.be/OGRK/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Provincie Limburg fietssnelwegen.be - Provincie Limburg](https://geo.limburg.be/arcgis/services/Mobiliteit_fietssnelwegen_be/MapServer/WMSServer) - [GetCapabilities](https://geo.limburg.be/arcgis/services/Mobiliteit_fietssnelwegen_be/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Provincie Limburg Mobiliteit Fiets - Provincie Limburg](https://geo.limburg.be/arcgis/services/Mobiliteit_fiets/MapServer/WMSServer) - [GetCapabilities](https://geo.limburg.be/arcgis/services/Mobiliteit_fiets/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Publiek_AtlasBuurtwegen - Provincie Antwerpen](https://gis.provincieantwerpen.be/arcgis/services/Publiek/AtlasBuurtwegen/MapServer/WMSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/AtlasBuurtwegen/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Publiek_AtlasWaterlopen - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/AtlasWaterlopen/MapServer/WMSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/AtlasWaterlopen/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Publiek_BFF_Wegenregister - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/BFF_Wegenregister/MapServer/WMSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/BFF_Wegenregister/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Publiek_Ms_AtlasWaterlopen - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/Ms_AtlasWaterlopen/MapServer/WMSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/Ms_AtlasWaterlopen/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Raadpleegdienst voor historische cartografie - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/histcart/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/histcart/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Raadpleegdienst voor luchtvaart - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/luchtvaart/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/luchtvaart/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Rivierbekkens - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/Rivierbekkens/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/Rivierbekkens/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Ruimtelijke ontwikkelingskansen Limburg - Provincie Limburg](https://geo.limburg.be/arcgis/services/klimaatadaptatie/Ontwikkelingskansenkaart_Limburg_bijgestuurd/MapServer/WMSServer) - [GetCapabilities](https://geo.limburg.be/arcgis/services/klimaatadaptatie/Ontwikkelingskansenkaart_Limburg_bijgestuurd/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RVV-Themabestand - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/RVVThemabestand/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/RVVThemabestand/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Signaalgebieden - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/Signaalgebieden/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/Signaalgebieden/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Topografische kaart 1:10000 kleur Oost-Vlaanderen - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/referentie/topo10k/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/referentie/topo10k/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Topografische kaart 1:10000 zwart-wit Oost-Vlaanderen - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/referentie/top10rzw/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/referentie/top10rzw/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Topografische kaart 1:100000 zwart-wit Oost-Vlaanderen - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/referentie/topo100zw/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/referentie/topo100zw/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Vervoersnetwerken - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/tn/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/tn/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [VHA_waterlopen - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/VHA_waterlopen/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/VHA_waterlopen/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [VHA_werkversie - geo.limburg.be](https://geo.limburg.be/arcgis/services/VHA_werkversie/MapServer/WMSServer) - [GetCapabilities](https://geo.limburg.be/arcgis/services/VHA_werkversie/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [VRB - gisservices.inbo.be](https://gisservices.inbo.be/arcgis/services/VRB/MapServer/WMSServer) - [GetCapabilities](https://gisservices.inbo.be/arcgis/services/VRB/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Water-Link - aquawardsoperate.water-link.be](https://aquawardsoperate.water-link.be/DrinkwaterSpatial/Service.svc/get) - [GetCapabilities](https://aquawardsoperate.water-link.be/DrinkwaterSpatial/Service.svc/get?REQUEST=GetCapabilities&SERVICE=WMS)

* [waterinfo_WFS - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/waterinfo_WFS/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/waterinfo_WFS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [waterlopen - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/waterlopen/MapServer/WMSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/waterlopen/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Watervlakken - gisservices.inbo.be](https://gisservices.inbo.be/arcgis/services/Watervlakken/MapServer/WMSServer) - [GetCapabilities](https://gisservices.inbo.be/arcgis/services/Watervlakken/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Web Map Service - GeoWebCache - Agentschap Wegen en Verkeer](https://opendata.apps.mow.vlaanderen.be/opendata-geowebcache/service/wms) - [GetCapabilities](https://opendata.apps.mow.vlaanderen.be/opendata-geowebcache/service/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Wegenregister - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Wegenregister/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/Wegenregister/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [Wijzigingen Atlaasen der Buurtwegen - Provincie Limburg - geo.limburg.be](https://geo.limburg.be/arcgis/services/ABW/MapServer/WMSServer) - [GetCapabilities](https://geo.limburg.be/arcgis/services/ABW/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Wijzigingen Atlas der Buurtwegen - Provincie West-Vlaanderen](https://www.geoloket.be/gwserver/services/Mobiliteit/AGS_Atlas_bw_wijziging/MapServer/WMSServer) - [GetCapabilities](https://www.geoloket.be/gwserver/services/Mobiliteit/AGS_Atlas_bw_wijziging/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Windturbines Oost-Vlaanderen - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/MIL/Windturbines/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/MIL/Windturbines/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geo.limburg.be](https://geo.limburg.be/arcgis/services/PinC/MapServer/WMSServer) - [GetCapabilities](https://geo.limburg.be/arcgis/services/PinC/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/organiccarboncontent/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/organiccarboncontent/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/waterbodyforwfdhorizon/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/waterbodyforwfdhorizon/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/activewell/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/activewell/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/aquifersystem/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/aquifersystem/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/drinkingwaterprotectionarea/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/drinkingwaterprotectionarea/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/physicalparameter/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/physicalparameter/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/AtlasWaterlopen/MapServer/WMSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/AtlasWaterlopen/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/RUPs/MapServer/WMSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/RUPs/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/waterinfo/VHA/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/waterinfo/VHA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodata.antwerpen.be](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek1/MapServer/WMSServer) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek1/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodata.antwerpen.be](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek2/MapServer/WMSServer) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek2/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodata.antwerpen.be](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek3/MapServer/WMSServer) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek3/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodata.antwerpen.be](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek4/MapServer/WMSServer) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek4/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/conepenetrationtest/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/conepenetrationtest/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodata.antwerpen.be](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek5/MapServer/WMSServer) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek5/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/basisdata/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/basisdata/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/Perimetergebieden_recht_van_voorverkoop/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/Perimetergebieden_recht_van_voorverkoop/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/Risicozones_overstroming_2017/MapServer/WMSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/Risicozones_overstroming_2017/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/gravitystation/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/gravitystation/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/Landschapsatlas/MapServer/WMSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/Landschapsatlas/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/PRUP/MapServer/WMSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/PRUP/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/SampleWorldCities/MapServer/WMSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/SampleWorldCities/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodata.antwerpen.be](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek6/MapServer/WMSServer) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek6/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodata.antwerpen.be](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek7/MapServer/WMSServer) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek7/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodata.antwerpen.be](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek8/MapServer/WMSServer) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek8/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodata.antwerpen.be](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek9/MapServer/WMSServer) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek9/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/exposedelement/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/exposedelement/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/geologicfault/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/geologicfault/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/MIL/Waarnemingen_Public/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/MIL/Waarnemingen_Public/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/MIL/atlaswaterlopen1/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/MIL/atlaswaterlopen1/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/MIL/atlaswaterlopen_geen_cache/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/MIL/atlaswaterlopen_geen_cache/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodiensten.oost-vlaanderen.be](https://geodiensten.oost-vlaanderen.be/arcgis/services/referentie/OSM/MapServer/WMSServer) - [GetCapabilities](https://geodiensten.oost-vlaanderen.be/arcgis/services/referentie/OSM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/BFF_Conformiteit/MapServer/WMSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/BFF_Conformiteit/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/Ms_Fietsdata/MapServer/WMSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/Ms_Fietsdata/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodata.antwerpen.be](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek10/MapServer/WMSServer) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek10/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geodata.antwerpen.be](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek11/MapServer/WMSServer) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/services/P_Portal/portal_publiek11/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geoserver-extern.ovam.be](https://geoserver-extern.ovam.be/geoserver/wms) - [GetCapabilities](https://geoserver-extern.ovam.be/geoserver/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/wms) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/observedsoilprofile/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/observedsoilprofile/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/mineraloccurrence/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/mineraloccurrence/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/landslide/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/landslide/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/aquitard/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/aquitard/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/waterbodyforwfd/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/waterbodyforwfd/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/biologicalparameter/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/biologicalparameter/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/geologicunit/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/geologicunit/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - gisservices.inbo.be](https://gisservices.inbo.be/arcgis/services/Genenbronnen/MapServer/WMSServer) - [GetCapabilities](https://gisservices.inbo.be/arcgis/services/Genenbronnen/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/SPI3/MapServer/WMSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/SPI3/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/SPI3_voorspeld/MapServer/WMSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/SPI3_voorspeld/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/extralagen/MapServer/WMSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/extralagen/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/gemeenten/MapServer/WMSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/gemeenten/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/polwat/MapServer/WMSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/polwat/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/waterinfo/Vrijboordkaarten/MapServer/WMSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/waterinfo/Vrijboordkaarten/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/chemicalparameter/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/chemicalparameter/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/environmentalmonitoringfacility/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/environmentalmonitoringfacility/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/aquifer/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/aquifer/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/mine/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/mine/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/borehole/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/borehole/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/soilsite/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/soilsite/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/naturalgeomorphologicfeature/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/naturalgeomorphologicfeature/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/boreholelogging/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/boreholelogging/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/prospectingandminingpermitarea/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/prospectingandminingpermitarea/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/phvalue/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/phvalue/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/flightline/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/flightline/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/seismicline/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/seismicline/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/soilbody/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/soilbody/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/riskzone/ows) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/riskzone/ows?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Basis_Services/Ms_Milieu/MapServer/WMSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Basis_Services/Ms_Milieu/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Basis_Services/Ms_activiteitenkaartDRP/MapServer/WMSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Basis_Services/Ms_activiteitenkaartDRP/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Adressenregister - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Adressenregister/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/Adressenregister/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Agentschap Facilitair Bedrijf - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/HFB/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/HFB/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Agentschap Innoveren en Ondernemen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VLAIO/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/VLAIO/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Agentschap voor Natuur en Bos - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/ANB/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/ANB/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Bodemgebruik en Bodembedekking - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/BodemgebruikBodembedekking/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/BodemgebruikBodembedekking/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Departement Landbouw en Visserij - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/ALV/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/ALV/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Digitaal Hoogtemodel Vlaanderen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/DHMV/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/DHMV/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Gebouwenregister - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Gebouwenregister/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/Gebouwenregister/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS GRB - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/GRB/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/GRB/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS GRB - Administratieve percelen fiscaal - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Adpf/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/Adpf/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS GRB-basiskaart - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/GRB-basiskaart/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/GRB-basiskaart/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS GRB-basiskaart grijs - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/GRB-basiskaart-grijs/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/GRB-basiskaart-grijs/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS GRB-selectie - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/GRB-selectie/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/GRB-selectie/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Instituut voor Natuur- en Bosonderzoek - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/INBO/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/INBO/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Jacht - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Jacht/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/Jacht/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS RVV Afbakeningen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/RVVAfbak/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/RVVAfbak/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Statistische Sectoren - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/StatistischeSectoren/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/StatistischeSectoren/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Stroomgebiedbeheerplannen - Vlaamse Milieumaatschappij](https://geoserver.vmm.be/geoserver/HDGIS/wms) - [GetCapabilities](https://geoserver.vmm.be/geoserver/HDGIS/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Toerisme Vlaanderen - Toerisme Vlaanderen](https://geodata.toerismevlaanderen.be/geoserver/wms) - [GetCapabilities](https://geodata.toerismevlaanderen.be/geoserver/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Universiteit Gent - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/UGent/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/UGent/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Verrijkte KruispuntBank Ondernemingen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VKBO/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/VKBO/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Vervoersmaatschappij De Lijn - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/DeLijn/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/DeLijn/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Vlaams Agentschap Zorg en Gezondheid - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VAZG/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/VAZG/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Vlaamse Landmaatschappij - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VLM/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/VLM/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Vlaamse Maatschappij voor Sociaal Wonen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VMSW/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/VMSW/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Vlaamse Milieumaatschappij - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VMM/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/VMM/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Wonen-Vlaanderen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/WonenVlaanderen/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/WonenVlaanderen/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Zorgatlas - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/zorgatlas/wms) - [GetCapabilities](https://geo.api.vlaanderen.be/zorgatlas/wms?REQUEST=GetCapabilities&SERVICE=WMS)



## Wallonia

* [/RES_ROUTIER_PROJETS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/RES_ROUTIER_PROJETS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/RES_ROUTIER_PROJETS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [/WALLONIE_MNS_2021_2022_HILLSHADE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNS_2021_2022_HILLSHADE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNS_2021_2022_HILLSHADE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [/WALLONIE_MNT_2021_2022_HILLSHADE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNT_2021_2022_HILLSHADE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNT_2021_2022_HILLSHADE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ACCES_BETAIL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/ACCES_BETAIL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/ACCES_BETAIL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ADESA - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/ADESA/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/ADESA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ADL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ADL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ADL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [AGRICULTURE_SIGEC_PARC_AGRI_ANON__2016 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2016/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2016/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [AGRO_GEO - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/AGRO_GEO/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/AGRO_GEO/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [AIRES_COVOITURES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/AIRES_COVOITURES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/AIRES_COVOITURES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ALEA_INOND - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/ALEA_INOND/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/ALEA_INOND/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ALEA_INOND_16_20__CHGT_ELEVE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/ALEA_INOND_16_20__CHGT_ELEVE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/ALEA_INOND_16_20__CHGT_ELEVE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ALIMENTATION_DURABLE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ALIMENTATION_DURABLE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ALIMENTATION_DURABLE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Aménagement foncier - Service de visualisation - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/AMGT_FONC/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/AMGT_FONC/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [AMENAGEMENT_TERRITOIRE_LOT - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/LOT/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/LOT/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [AOT_ARRETBOX - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/AOT_ARRETBOX/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/AOT_ARRETBOX/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [AOT_ARRETTEC - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/AOT_ARRETTEC/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/AOT_ARRETTEC/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [AOT_BANDESBUS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/AOT_BANDESBUS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/AOT_BANDESBUS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [AOT_MOBIPOLE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/AOT_MOBIPOLE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/AOT_MOBIPOLE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [AOT_RESEAUFER - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/AOT_RESEAUFER/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/AOT_RESEAUFER/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [APP_BDES_BDES_STATUT - Service public de Wallonie](https://geoservices3.wallonie.be/arcgis/services/APP_BDES/BDES_STATUT/MapServer/WMSServer) - [GetCapabilities](https://geoservices3.wallonie.be/arcgis/services/APP_BDES/BDES_STATUT/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ART_DIV22 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/ART_DIV22/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/ART_DIV22/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Atlas des voiries vicinales - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/PLAN_REGLEMENT/ATLAS_VV_MODIF/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/PLAN_REGLEMENT/ATLAS_VV_MODIF/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ATLAS HYDRO - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/ATLAS_HYDRO/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/ATLAS_HYDRO/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BASSINS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/BASSINS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/BASSINS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BASSINS_ORAGE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/BASSINS_ORAGE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/BASSINS_ORAGE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Bathymétrie - Voies hydrauliques wallonnes - Service de visualisation WMS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/BATHY/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/BATHY/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BC_PAT - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/BC_PAT/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/BC_PAT/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BDES_INVENTAIRE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/BDES_INVENTAIRE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/BDES_INVENTAIRE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BDOA - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/BDOA/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/BDOA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BERCE_CAUCASE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/BERCE_CAUCASE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/BERCE_CAUCASE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BIOCLIM - Service publique de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/BIOCLIM/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/BIOCLIM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BRUIT_2006_RAIL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_2006_RAIL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_2006_RAIL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BRUIT_2006_ROAD - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_2006_ROAD/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_2006_ROAD/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BRUIT_AEROPORTS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_AEROPORTS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_AEROPORTS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BRUIT_AGGLO_IND_2012 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_AGGLO_IND_2012/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_AGGLO_IND_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BRUIT_AGGLO_RAIL_2012 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_AGGLO_RAIL_2012/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_AGGLO_RAIL_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BRUIT_AGGLO_ROAD_2012 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_AGGLO_ROAD_2012/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_AGGLO_ROAD_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BRUIT_MRAIL_2017 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_MRAIL_2017/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_MRAIL_2017/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [BRUIT_MROAD_2017 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_MROAD_2017/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/BRUIT_MROAD_2017/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CADASPORTS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/CADASPORTS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/CADASPORTS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Cadastre des antennes - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ANTENNES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ANTENNES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CADMAP_2021_PARCELLES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/PLAN_REGLEMENT/CADMAP_2021_PARCELLES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/PLAN_REGLEMENT/CADMAP_2021_PARCELLES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CADMAP_2022_PARCELLES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/PLAN_REGLEMENT/CADMAP_2022_PARCELLES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/PLAN_REGLEMENT/CADMAP_2022_PARCELLES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CADMAP_2023 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/PLAN_REGLEMENT/CADMAP_2023_PARCELLES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/PLAN_REGLEMENT/CADMAP_2023_PARCELLES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CAMBIO - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/CAMBIO/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/CAMBIO/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CAMPING - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/CAMPING/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/CAMPING/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CAPTAGES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/CAPTAGES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/CAPTAGES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CARBIOSOL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CARBIOSOL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CARBIOSOL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Carte d´occupation du sol de Wallonie version 2_07 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/OCCUPATION_SOL/COSW2_07/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/OCCUPATION_SOL/COSW2_07/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CARTES_GEOMORPHO - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CARTES_GEOMORPHO/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CARTES_GEOMORPHO/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CAW - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/CAW/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/CAW/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CCATM - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/CCATM/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/CCATM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Centres Publics d’Action Sociale (CPAS) de Wallonie Francophone - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/CPAS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/CPAS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CENT_HYDRO_ELEC - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/CENT_HYDRO_ELEC/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/CENT_HYDRO_ELEC/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CHASSE_TERRIT_ANONY - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/CHASSE_TERRIT_ANONYM/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/CHASSE_TERRIT_ANONYM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Circulation des poissons - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/CIRC_POISSONS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/CIRC_POISSONS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CNSW - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CNSW/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CNSW/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CNSW__PRINC_TYPES_SOLS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CNSW__PRINC_TYPES_SOLS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CNSW__PRINC_TYPES_SOLS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [COMDEC - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/COMDEC/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/COMDEC/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CONCESSIONS_MINES_SITUATION - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CONCESSIONS_MINES_SITUATION_ADMIN/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CONCESSIONS_MINES_SITUATION_ADMIN/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CONCESSIONS_MINES_SUBSATNCES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CONCESSIONS_MINES_SUBSTANCES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CONCESSIONS_MINES_SUBSTANCES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CONSNAT - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/CONSNAT/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/CONSNAT/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CONSULT_SSOL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CONSULT_SSOL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CONSULT_SSOL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CONS_CYN - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/CONS_CYN/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/CONS_CYN/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Contraintes liées au risque d’éboulements de parois rocheuses - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/RISQ_EBOULT/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/RISQ_EBOULT/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CONTR_KARST - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/CONTR_KARST/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/CONTR_KARST/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CONT_RIV - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/CONT_RIV/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/CONT_RIV/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [COUTOURLINES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IGN/CONTOURLINES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IGN/CONTOURLINES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [DECHETS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/DECHETS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/DECHETS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [DECHETS_MINIERS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/DECHETS_MINIERS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/DECHETS_MINIERS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [DENSPOP_TYPO - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/HABITAT/DENSPOP_TYPO/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/HABITAT/DENSPOP_TYPO/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [DEPOT_GUERRE_1865_1880 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/CARTES_ANCIENNES/DEPOT_GUERRE_1865_1880/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/CARTES_ANCIENNES/DEPOT_GUERRE_1865_1880/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Districts et secteurs administratifs de gestion de l'eau - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/LIM_ADMIN_DCENN/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/LIM_ADMIN_DCENN/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [DONNEES_BASE_FDC_SPW - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/DONNEES_BASE/FDC_SPW/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/DONNEES_BASE/FDC_SPW/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [DONNEES_BASE_FDC_SPW_ANNOTATIONS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/DONNEES_BASE/FDC_SPW_ANNOTATIONS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/DONNEES_BASE/FDC_SPW_ANNOTATIONS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [DONNEES_BASE_FDC_SPW_LEGER - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/DONNEES_BASE/FDC_SPW_LEGER/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/DONNEES_BASE/FDC_SPW_LEGER/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EAU_MOULINS_HYDRO - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/MOULINS_HYDRO/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/MOULINS_HYDRO/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EAU_RES_EAU_QUAL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/RES_EAU_QUAL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/RES_EAU_QUAL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EAU_RHW_SIMPLE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/RHW_SIMPLE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/RHW_SIMPLE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ECOLES_NUMERIQUES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ECOLES_NUMERIQUES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ECOLES_NUMERIQUES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ENDETTEMENT_ENERGIE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ENDETTEMENT_ENERGIE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ENDETTEMENT_ENERGIE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ENERGIE_GEOTHERMIE_DEMANDES_CHALEUR_FROID_RESIDENTIEL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/ENERGIE/GEOTHERMIE_DEMANDES_CHALEUR_FROID_RESIDENTIEL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/ENERGIE/GEOTHERMIE_DEMANDES_CHALEUR_FROID_RESIDENTIEL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ENTREPRISES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ENTREPRISES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ENTREPRISES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EN_RET - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/EN_RET/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/EN_RET/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EP - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/EP/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/EP/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EPN - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/EPN/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/EPN/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EPRI - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/EPRI/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/EPRI/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ERRUISSOL - Risque d´érosion hydrique diffuse - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/ERRUISSOL__EROSION_DIFFUSE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/ERRUISSOL__EROSION_DIFFUSE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ERRUISSOL - Risque de ruissellement diffus - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/ERRUISSOL__RUISSELLEMENT_DIFFUS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/ERRUISSOL__RUISSELLEMENT_DIFFUS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ESPACE_WALLONIE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ESPACE_WALLONIE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ESPACE_WALLONIE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ESP_LIGNEUX - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/ESP_LIGNEUX/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/ESP_LIGNEUX/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Etablissements pour les aînés - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ETAB_AINES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ETAB_AINES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [FAUNE_FLORE_AHREM - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/AHREM/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/AHREM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [FAUNE_FLORE_BDR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/BDR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/BDR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [FAUNE_FLORE_FEE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/FEE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/FEE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [FAUNE_FLORE_LIMITES_DNF - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/LIMITES_DNF/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/LIMITES_DNF/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [FAUNE_FLORE_NATURA2000 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/NATURA2000/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/NATURA2000/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [FERRARIS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/CARTES_ANCIENNES/FERRARIS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/CARTES_ANCIENNES/FERRARIS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [FORET - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FORET/FORET/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FORET/FORET/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [FORETANC - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FORET/FORETANC/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FORET/FORETANC/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [GCU - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/GCU/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/GCU/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [GEOLOGIE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/GEOLOGIE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/GEOLOGIE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [GEOTECH - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/GEOTECH/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/GEOTECH/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [GEOTHERMIE_POTENTIEL_NET_SYSTEME_RESIDENTIEL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/ENERGIE/GEOTHERMIE_POTENTIEL_NET_SYSTEME_RESIDENTIEL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/ENERGIE/GEOTHERMIE_POTENTIEL_NET_SYSTEME_RESIDENTIEL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [GEOTHERMIE_POTENTIEL_NET_SYSTEME_TERTIARE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/ENERGIE/GEOTHERMIE_POTENTIEL_NET_SYSTEME_TERTIARE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/ENERGIE/GEOTHERMIE_POTENTIEL_NET_SYSTEME_TERTIARE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [GEOTHERMIE_POTENTIEL_TECHNIQUE_SYSTEMES_FERMES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/ENERGIE/GEOTHERMIE_POTENTIEL_TECHNIQUE_SYSTEMES_FERMES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/ENERGIE/GEOTHERMIE_POTENTIEL_TECHNIQUE_SYSTEMES_FERMES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [GEOTHERMIE_POTENTIEL_TECHNIQUE_SYSTEMES_OUVERTS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/ENERGIE/GEOTHERMIE_POTENTIEL_TECHNIQUE_SYSTEMES_OUVERTS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/ENERGIE/GEOTHERMIE_POTENTIEL_TECHNIQUE_SYSTEMES_OUVERTS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [GR sentiers - qgiscloud.com](https://qgiscloud.com/grsentiers/SGR_Reseau_Public/wms) - [GetCapabilities](https://qgiscloud.com/grsentiers/SGR_Reseau_Public/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [GRU - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/GRU/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/GRU/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HALL_RELAIS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/HALL_RELAIS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/HALL_RELAIS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [IAAS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/IAAS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/IAAS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [IMAGERIE_ORTHO_2009_2010 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2009_2010/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2009_2010/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [IMAGERIE_ORTHO_2012_2013 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2012_2013/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2012_2013/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [IMAGERIE_ORTHO_MAILLAGE_TUILAGE_1978_1990 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_1978_1990/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_1978_1990/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [INDUSTRIES_SERVICES_JEUNES_ODD - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/JEUNES_ODD/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/JEUNES_ODD/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [INDUSTRIES_SERVICES_SEVESO - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/SEVESO/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/SEVESO/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE - Altitude en Wallonie (BE) - Service de visualisation WMS - Service public de Wallonie](https://geoservices.wallonie.be/INSPIRE/WMS/EL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/INSPIRE/WMS/EL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE - Orthophotographie de Wallonie (BE) - Service de visualisation WMS - Service public de Wallonie](https://geoservices.wallonie.be/INSPIRE/WMS/OI/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/INSPIRE/WMS/OI/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [INSPIRE - Réseaux de transport en Wallonie (BE) - Service de visualisation WMS - Service public de Wallonie](https://geoservices.wallonie.be/INSPIRE/WMS/TN/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/INSPIRE/WMS/TN/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [IPE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/IPE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/IPE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [IPIC - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/IPIC/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/IPIC/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ISA - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/ISA/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/ISA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [KARST - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/KARST/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/KARST/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LCPI - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/LCPI/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/LCPI/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LEGIS_PECHE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/LEGIS_PECHE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/LEGIS_PECHE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Les bassins hydrographiques - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/BASSINS__BASSINS_PG/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/BASSINS__BASSINS_PG/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LIDAR_MAILLES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/LIDAR_MAILLES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/LIDAR_MAILLES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LIDAXES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/LIDAXES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/LIDAXES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Limites des régions agricoles belges - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/LIMITES/LIMITES_REGIONSAGRICOLES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/LIMITES/LIMITES_REGIONSAGRICOLES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LIMITES_ADMINISTRATIVES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/LIMITES/LIMITES_ADMINISTRATIVES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/LIMITES/LIMITES_ADMINISTRATIVES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LIMITES_ADMIN_DPA - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/LIMITES/LIMITES_ADMIN_DPA/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/LIMITES/LIMITES_ADMIN_DPA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LIMITES_QS_STATBEL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/LIMITES/LIMITES_QS_STATBEL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/LIMITES/LIMITES_QS_STATBEL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LIMITES_QUARTIERS_IWEPS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/LIMITES/LIMITES_QUARTIERS_IWEPS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/LIMITES/LIMITES_QUARTIERS_IWEPS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LIM_ADMIN_DEE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/LIM_ADMIN_DEE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/LIM_ADMIN_DEE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Localisation des centres CREAVES en Wallonie - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/CREAVES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/CREAVES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LOUP_ZIPP - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/LOUP_ZIPP/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/LOUP_ZIPP/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LUTTE_INOND - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/LUTTE_INOND/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/LUTTE_INOND/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LW_Belgium_ecotopes - maps.elie.ucl.ac.be](https://maps.elie.ucl.ac.be/cgi-bin/mapserv72?map=/maps_server/lifewatch/mapfiles/LW_Belgium_ecotopes_lc_raster.map) - [GetCapabilities](https://maps.elie.ucl.ac.be/cgi-bin/mapserv72?map=/maps_server/lifewatch/mapfiles/LW_Belgium_ecotopes_lc_raster.map?REQUEST=GetCapabilities&SERVICE=WMS)

* [L_ECOLO - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/L_ECOLO/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/L_ECOLO/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [MASSES_EAU_SOUT_2017 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/MASSES_EAU_SOUT_2017/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/MASSES_EAU_SOUT_2017/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [MASSIFS_FORESTIERS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/TOURISME/MASSIFS_FORESTIERS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/TOURISME/MASSIFS_FORESTIERS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [MDT - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/TOURISME/MDT/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/TOURISME/MDT/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [MESU - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/MESU/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/MESU/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [MOBILITE_CPDT - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/CPDT/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/CPDT/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [MOBILITE_VOIESNAVIGABLES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/VOIESNAVIGABLES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/VOIESNAVIGABLES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [MODIF_PS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/MODIF_PS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/MODIF_PS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [NITRATES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/NITRATES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/NITRATES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [OFFRES_TOURISTIQUES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/TOURISME/OFFRES_TOURISTIQUES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/TOURISME/OFFRES_TOURISTIQUES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthophotos 1994-2000 - Maillage - Service de visualisation - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_1994_2000/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_1994_2000/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthophotos 2006-207 - Maillage - Service de visualisation - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_2006_2007/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_2006_2007/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthophotos 2009-2010 - Maillage et tuilage - Service de visualisation - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2009_2010/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2009_2010/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthophotos 2012-2013 - Maillage et tuilage - Service de visualisation WMS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2012_2013/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2012_2013/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthophotos 2015 - Maillage et tuilage - Service de visualisation WMS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2015/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthophotos 2016 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2016/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2016/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthophotos 2016 - IR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2016_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2016_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthophotos 2016 - Maillage et tuilage - Service de visualisation WMS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2016/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2016/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Orthophotos_1994_2000 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_1994_2000/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_1994_2000/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_1971 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_1971/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_1971/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_1978_1990 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_1978_1990/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_1978_1990/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2001_2003 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2001_2003/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2001_2003/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2006_2007 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2006_2007/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2006_2007/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2015 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2015/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2015_IR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2015_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2015_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2017 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2017/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2017/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2017_IR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2017_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2017_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2018 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2018/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2018/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2018_IR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2018_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2018_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2019 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2019/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2019/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2019_IR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2019_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2019_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2020 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2020/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2020/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2020_IR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2020_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2020_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2021 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2021/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2021/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2021_IR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2021_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2021_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2022_ETE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2022_ETE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2022_ETE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2022_ETE_IR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2022_ETE_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2022_ETE_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2022_PRINTEMPS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2022_PRINTEMPS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2022_PRINTEMPS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2022_PRINTEMPS_IR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2022_PRINTEMPS_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2022_PRINTEMPS_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2023_ETE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2023_ETE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2023_ETE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_2023_ETE_IR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2023_ETE_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2023_ETE_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_FOOTPRINT_2022_ETE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_FOOTPRINT_2022_ETE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_FOOTPRINT_2022_ETE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_FOOTPRINT_2022_PRINTEMPS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_FOOTPRINT_2022_PRINTEMPS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_FOOTPRINT_2022_PRINTEMPS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_FOOTPRINT_2023_ETE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_FOOTPRINT_2023_ETE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_FOOTPRINT_2023_ETE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_LAST - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_LAST/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_LAST/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_LAST_IR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_LAST_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_LAST_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_MAILLAGE_TUILAGE_1971 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_1971/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_1971/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_MAILLAGE_TUILAGE_2017 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2017/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2017/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_MAILLAGE_TUILAGE_2018 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2018/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2018/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_MAILLAGE_TUILAGE_2019 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2019/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2019/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_MAILLAGE_TUILAGE_2020 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2020/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2020/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_MAILLAGE_TUILAGE_2021 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2021/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2021/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_MAILLAGE_TUILAGE_2022_ETE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2022_ETE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2022_ETE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_MAILLAGE_TUILAGE_2022_PRINTEMPS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2022_PRINTEMPS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2022_PRINTEMPS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ORTHO_MAILLAGE_TUILAGE_2023_ETE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2023_ETE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_MAILLAGE_TUILAGE_2023_ETE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Parcs nationaux de Wallonie - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/PARCS_NATIONAUX/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/PARCS_NATIONAUX/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PAT_EXC - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PAT_EXC/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PAT_EXC/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PAT_LSTSAV - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PAT_LSTSAV/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PAT_LSTSAV/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PAT_MND_UNESCO - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PAT_MND_UNESCO/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PAT_MND_UNESCO/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PCAML14_16 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PCAML14_16/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PCAML14_16/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PCS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/PCS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/PCS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PESTE_PA - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/PESTE_PA/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FAUNE_FLORE/PESTE_PA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Plan de secteur en vigueur (version coordonn\xe9e vectorielle) - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PDS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PDS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PLANHIVER_TRONCONS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/PLANHIVER_TRONCONS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/PLANHIVER_TRONCONS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PLANHP - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PLANHP/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PLANHP/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PLANS_MOBILITE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/PLANS_MOBILITE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/PLANS_MOBILITE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PLANS_TRAVAUXPUBLICS_1950_1973 - Service Public de Wallonie](https://geoservices.wallonie.be/arcgis/services/TOPOGRAPHIE/PLANS_TRAVAUXPUBLICS_1950_1973/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/TOPOGRAPHIE/PLANS_TRAVAUXPUBLICS_1950_1973/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [POT_BORNES_RECHARG - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/POT_BORNES_RECHARG/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/POT_BORNES_RECHARG/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PPR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PPR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PPR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PRE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PRE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PRE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Projet Informatique de Cartographie Continue (PICC) - Service de visualisation - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/TOPOGRAPHIE/PICC_VDIFF/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/TOPOGRAPHIE/PICC_VDIFF/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PROTEC_CAPT - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/PROTECT_CAPT/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/PROTECT_CAPT/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [PRWE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PRWE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/PRWE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Réseau des espaces de coworking wallons (CoWallonia) - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/COWORKING_DW/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/COWORKING_DW/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Réseau routier régional - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/RES_ROUTIER_REGIONAL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/RES_ROUTIER_REGIONAL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RAVeL et Véloroutes en Wallonie - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/RAVEL_VELOROUTES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/RAVEL_VELOROUTES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Relief de la Wallonie - Modèle Numérique de Surface (MNS) 2013-2014 – Hillshade (Service de visualisation) - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNS_2013_2014_HILLSHADE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNS_2013_2014_HILLSHADE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Relief de la Wallonie - Modèle Numérique de Surface (MNS) 2013-2014 (Service de visualisation) - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNS_2013_2014/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNS_2013_2014/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Relief de la Wallonie - Modèle Numérique de Surface (MNS) 2013-2014 - Densité des mesures (Service de visualisation) - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNS_2013_2014_DENSITE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNS_2013_2014_DENSITE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Relief de la Wallonie - Modèle Numérique de Terrain (MNT) 2013-2014 (Service de visualisation) - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNT_2013_2014/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNT_2013_2014/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Relief de la Wallonie - Modèle Numérique de Terrain (MNT) 2013-2014 - Hillshade (Service de visualisation) - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNT_2013_2014_HILLSHADE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNT_2013_2014_HILLSHADE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Relief de la Wallonie 2013-2014 - Vue simple (Service de visualisation) - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_2013_2014_VUE_SIMPLE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_2013_2014_VUE_SIMPLE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RELIEF_WALLONIE_MNP_2013_2014__PENTES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNP_2013_2014__PENTES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNP_2013_2014__PENTES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RELIEF_WALLONIE_MNP_2013_2014__RUPTURES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNP_2013_2014__RUPTURES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNP_2013_2014__RUPTURES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RELIEF_WALLONIE_MNS_2021_2022 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNS_2021_2022/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNS_2021_2022/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RELIEF_WALLONIE_MNT_2021_2022 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNT_2021_2022/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNT_2021_2022/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RENOV_URB - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/RENOV_URB/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/RENOV_URB/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RES_PIEZO - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/RES_PIEZO/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/RES_PIEZO/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RES_ROUTIER_POT_ECL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/RES_ROUTIER_POT_ECL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/RES_ROUTIER_POT_ECL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RES_ROUTIER_REGIONAL_PMQ - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/RES_ROUTIER_REGIONAL_PMQ/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/RES_ROUTIER_REGIONAL_PMQ/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RES_ROUTIER_REGIONAL_THEMATIQUE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/RES_ROUTIER_REGIONAL_THEMATIQUE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/RES_ROUTIER_REGIONAL_THEMATIQUE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [REVIT_URB - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/REVIT_URB/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/REVIT_URB/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RHW - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/RHW/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/RHW/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RI - Service public de Wallonie](https://geoservices3.wallonie.be/arcgis/services/APP_ALINO/RI/MapServer/WMSServer) - [GetCapabilities](https://geoservices3.wallonie.be/arcgis/services/APP_ALINO/RI/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RMBMT_URB - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/RMBMT_URB/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/RMBMT_URB/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SAPINS_DE_NOEL_2015 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SAPINS_DE_NOEL_2015/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SAPINS_DE_NOEL_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SAR - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/SAR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/SAR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SDC - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/SDC/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/SDC/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SDCW - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/SDCW/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/SDCW/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SDT - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/SDT/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/SDT/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Secteurs attribués aux piegeurs de rats musqués - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/RATS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/RATS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Secteurs PARIS et bassins versant contributifs des secteurs PARIS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/SECTEURS_PARIS_1621/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/SECTEURS_PARIS_1621/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Sentiers géologiques et pédologiques de la province de Namur - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/SGP/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/SGP/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SERVICES_URGENCE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/LIMITES/SERVICES_URGENCE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/LIMITES/SERVICES_URGENCE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SIGEC_PARC_AGRI_ANON - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SIGEC_PARC_AGRI_ANON__2015 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2015/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SIGEC_PARC_AGRI_ANON__2017 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2017/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2017/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SIGEC_PARC_AGRI_ANON__2018 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2018/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2018/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SIGEC_PARC_AGRI_ANON__2019 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2019/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2019/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SIGEC_PARC_AGRI_ANON__2020 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2020/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2020/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SIGEC_PARC_AGRI_ANON__2021 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2021/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2021/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SIGEC_PARC_AGRI_ANON__2022 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2022/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2022/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SOIGNES_VANDERSTOCK_1661 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/CARTES_ANCIENNES/SOIGNES_VANDERSTOCK_1661/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/CARTES_ANCIENNES/SOIGNES_VANDERSTOCK_1661/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SOL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/SOL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/SOL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SOL_EAU - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/SOL_EAU/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/SOL_EAU/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SOL_SOUS_SOL_CONTEXTES_ECOLOGIQUES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CONTEXTES_ECOLOGIQUES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CONTEXTES_ECOLOGIQUES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SOL_SOUS_SOL_EPS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/EPS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/EPS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SOL_SOUS_SOL_SES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/SES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/SES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SOL_SOUS_SOL_WAL_OCS_IA__CHA_18_20 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/WAL_OCS_IA__CHA_18_20/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/WAL_OCS_IA__CHA_18_20/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Stations du réseau limnimétrique de la DGO3 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/RES_LIMNI_DGARNE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/RES_LIMNI_DGARNE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [SURFACES_CARRIERES_ACTIVES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/SURFACES_CARRIERES_ACTIVES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/SURFACES_CARRIERES_ACTIVES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [TBP - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/TBP/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/TBP/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [TERRILS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/TERRILS/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/TERRILS/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [TERRILS_2003 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/TERRILS_2003/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/TERRILS_2003/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [TERRILS_2018 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/TERRILS_2018/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/TERRILS_2018/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [TEXT - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/TEXT/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/TEXT/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [TISSU_URBANISE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/HABITAT/TISSU_URBANISE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/HABITAT/TISSU_URBANISE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [TNU_InZH - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/HABITAT/TNU_INZH/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/HABITAT/TNU_INZH/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [VDML - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/CARTES_ANCIENNES/VDML/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/CARTES_ANCIENNES/VDML/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WALLONIE_MNP_2013_2014__CLASSES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNP_2013_2014__CLASSES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/RELIEF/WALLONIE_MNP_2013_2014__CLASSES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WALOUS_OCCUPATION_SOL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/WALOUS_OCCUPATION_SOL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/WALOUS_OCCUPATION_SOL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WALOUS_UTILISATION_SOL - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/WALOUS_UTILISATION_SOL/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/WALOUS_UTILISATION_SOL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WAL_OCS_IA__2019 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/WAL_OCS_IA__2019/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/WAL_OCS_IA__2019/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WAL_OCS_IA__CHA_18_19 - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/WAL_OCS_IA__CHA_18_19/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/WAL_OCS_IA__CHA_18_19/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Wateringues - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/WATERINGUES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/WATERINGUES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WebPash_WebPASH - sig.spge.be](https://sig.spge.be/server/services/WebPash/WebPASH/MapServer/WMSServer) - [GetCapabilities](https://sig.spge.be/server/services/WebPash/WebPASH/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/MOBILITE/LIGNES_SNCB_SNCV_DESAFFECT/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/MOBILITE/LIGNES_SNCB_SNCV_DESAFFECT/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2012_2013_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2012_2013_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geoservices2.wallonie.be](https://geoservices2.wallonie.be/arcgis/services/APP_MOBILITE/BREVETCYCLISTE/MapServer/WMSServer) - [GetCapabilities](https://geoservices2.wallonie.be/arcgis/services/APP_MOBILITE/BREVETCYCLISTE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IGN/TOP10s_NB/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IGN/TOP10s_NB/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geoservices2.wallonie.be](https://geoservices2.wallonie.be/arcgis/services/APP_MOBILITE/ATELIERVELO/MapServer/WMSServer) - [GetCapabilities](https://geoservices2.wallonie.be/arcgis/services/APP_MOBILITE/ATELIERVELO/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Service public de Wallonie](https://geoservices3.wallonie.be/arcgis/services/APP_BDES/BDES_Identify_RC1/MapServer/WMSServer) - [GetCapabilities](https://geoservices3.wallonie.be/arcgis/services/APP_BDES/BDES_Identify_RC1/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Service public de Wallonie](https://geoservices3.wallonie.be/arcgis/services/APP_BDES/BDES_PANIER/MapServer/WMSServer) - [GetCapabilities](https://geoservices3.wallonie.be/arcgis/services/APP_BDES/BDES_PANIER/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Service public de Wallonie](https://geoservices3.wallonie.be/arcgis/services/APP_BDES/WALTERRE/MapServer/WMSServer) - [GetCapabilities](https://geoservices3.wallonie.be/arcgis/services/APP_BDES/WALTERRE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/ENERGIE/GEOTHERMIE_DEMANDES_CHALEUR_FROID_TERTIAIRE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/ENERGIE/GEOTHERMIE_DEMANDES_CHALEUR_FROID_TERTIAIRE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FDC/FDC_SPW_LEGER/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FDC/FDC_SPW_LEGER/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/FDC/ORTHO_LAST/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/FDC/ORTHO_LAST/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS de la Carte géologique de Wallonie - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CARTE_GEOLOGIQUE_SIMPLE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/SOL_SOUS_SOL/CARTE_GEOLOGIQUE_SIMPLE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS Orthophotos 2009-2010 Infrarouge - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2009_2010_IR/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2009_2010_IR/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ZDE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ZDE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ZDE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ZI - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/ZI/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/ZI/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ZIP - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/ZIP/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/AMENAGEMENT_TERRITOIRE/ZIP/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Zones de baignade et zones amonts - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/BAIGNADE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/BAIGNADE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ZONES_DEVELOPPEMENT - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ZONES_DEVELOPPEMENT/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ZONES_DEVELOPPEMENT/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ZONES_INONDEES - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/ZONES_INONDEES/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/ZONES_INONDEES/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ZONES_INONDEES_IDW - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/EAU/ZONES_INONDEES_IDW/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/EAU/ZONES_INONDEES_IDW/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [ZONE_FRANCHE - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ZONE_FRANCHE/MapServer/WMSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/INDUSTRIES_SERVICES/ZONE_FRANCHE/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)



## World

* [2007_2012_NOISE_END_LAEA_Exposure_Changes - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2007_2012_NOISE_END_LAEA_Exposure_Changes/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2007_2012_NOISE_END_LAEA_Exposure_Changes/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [2007_2012_NOISE_END_LAEA_Exposure_Changes - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_update2015_Exposure_Changes/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_update2015_Exposure_Changes/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [2007_NOISE_END_LAEA_Contours - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2007_NOISE_END_LAEA_Contours/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2007_NOISE_END_LAEA_Contours/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [2007_NOISE_END_LAEA_Update2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2007_NOISE_END_LAEA_Update2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2007_NOISE_END_LAEA_Update2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [2007_NOISE_END_LAEA_Update2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2007_NOISE_END_LAEA_update2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2007_NOISE_END_LAEA_update2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [2012_NOISE_END_LAEA_Major_Airports - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_Major_Airports/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_Major_Airports/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [2012_NOISE_END_LAEA_Major_Airports - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_update2015_Major_Airports/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_update2015_Major_Airports/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [2012_NOISE_END_LAEA_update2015_Major_Railways - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_update2015_Major_Railways/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_update2015_Major_Railways/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [2012_NOISE_END_LAEA_Within_Agglomerations - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_Within_Agglomerations/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_Within_Agglomerations/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [2012_NOISE_END_LAEA_Within_Agglomerations - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_update2015_Within_Agglo/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_update2015_Within_Agglo/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Ahocevar Geospatial Base Maps - ahocevar geospatial](https://ahocevar.com/geoserver/wms) - [GetCapabilities](https://ahocevar.com/geoserver/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [all_categories_pgn - EEA](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/Noise_all_categories_pgn/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/Noise_all_categories_pgn/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CLC2006_WM - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2006_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2006_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CLC2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [CLC2012_WM - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2012_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2012_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Airbase/O3_interpolated_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Airbase/O3_interpolated_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Airbase/PM10_interpolated_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Airbase/PM10_interpolated_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Airbase/PM2_5_interpolated_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Airbase/PM2_5_interpolated_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/EUHydro/Coastal/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/EUHydro/Coastal/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/EUHydro/Drainage/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/EUHydro/Drainage/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/EUHydro/RiverBasine/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/EUHydro/RiverBasine/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/2012_Coverage1_20m/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/2012_Coverage1_20m/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/Core01Cov2_2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/Core01Cov2_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/HRIM2012_C2_5/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/HRIM2012_C2_5/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/Image2009_Cov1/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/Image2009_Cov1/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/VeryHighResolution2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/VeryHighResolution2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/Lucas2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/Lucas2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Image/Image2006Cov1_201608022051/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Image/Image2006Cov1_201608022051/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Core01Cov2_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Image/Image2006Cov2/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Image/Image2006Cov2/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover 1990 WM - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC1990/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC1990/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover 1990 WM - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC1990_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC1990_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover 2000 LAEA - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2000/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2000/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover 2000 WM - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2000_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2000_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover 2006 LAEA - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2006/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2006/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover 2018 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2018_LAEA/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2018_LAEA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover 2018 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2018_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CLC2018_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover Change 1990-2000 LAEA - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2000/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2000/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover Change 1990-2000 WM - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA1990_2000_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA1990_2000_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover Change 2000-2006 LAEA - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2006/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2006/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover Change 2000-2006 WM - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2000_2006_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2000_2006_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover Change 2006-2012 LAEA - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover Change 2006-2012 WM - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2006_2012_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2006_2012_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover Change 2012-2018 LAEA - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2012_2018_LAEA/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2012_2018_LAEA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Corine Land Cover Change 2012-2018 WM - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2012_2018_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Corine/CHA2012_2018_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EEA-NOISE VIEWER - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/Noise_Dyna_LAEA/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/Noise_Dyna_LAEA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EMS2012_Rel2017_WM - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/ESM2012_Rel2017_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/ESM2012_Rel2017_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EO Browser - Sentinel-2 - Sinergise d.o.o.](https://eocloud.sentinel-hub.com/v1/wms/cd8471e2-eeb4-48da-9df5-ab9fdfedda30) - [GetCapabilities](https://eocloud.sentinel-hub.com/v1/wms/cd8471e2-eeb4-48da-9df5-ab9fdfedda30?REQUEST=GetCapabilities&SERVICE=WMS)

* [EOX::Maps - tiles.maps.eox.at](https://tiles.maps.eox.at/) - [GetCapabilities](https://tiles.maps.eox.at/?REQUEST=GetCapabilities&SERVICE=WMS)

* [EUHydro_Coastline - EEA](https://image.discomap.eea.europa.eu/arcgis/services/EUHydro/EUHydro_Coastline/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/EUHydro/EUHydro_Coastline/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EUHydro_RiverNetworkDatabase - EEA](https://image.discomap.eea.europa.eu/arcgis/services/EUHydro/EUHydro_RiverNetworkDatabase/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/EUHydro/EUHydro_RiverNetworkDatabase/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [EUHydro_RiverNetworkDatabase_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/EUHydro_RiverNetworkDatabase_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/EUHydro_RiverNetworkDatabase_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Fake Service - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/EEATravel_Dyna_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/EEATravel_Dyna_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Geofabrik OpenStreetMap WMS - full.wms.geofabrik.de](https://full.wms.geofabrik.de/std/demo_key) - [GetCapabilities](https://full.wms.geofabrik.de/std/demo_key?REQUEST=GetCapabilities&SERVICE=WMS)

* [Geofabrik Tools: OSM Inspector (Addresses) - Geofabrik GmbH](https://tools.geofabrik.de/osmi/views/addresses/wxs) - [GetCapabilities](https://tools.geofabrik.de/osmi/views/addresses/wxs?REQUEST=GetCapabilities&SERVICE=WMS)

* [Geofabrik Tools: OSM Inspector (Areas) - Geofabrik GmbH](https://tools.geofabrik.de/osmi/views/areas/wxs) - [GetCapabilities](https://tools.geofabrik.de/osmi/views/areas/wxs?REQUEST=GetCapabilities&SERVICE=WMS)

* [Geofabrik Tools: OSM Inspector (Highways) - Geofabrik GmbH](https://tools.geofabrik.de/osmi/views/highways/wxs) - [GetCapabilities](https://tools.geofabrik.de/osmi/views/highways/wxs?REQUEST=GetCapabilities&SERVICE=WMS)

* [Geofabrik Tools: OSM Inspector (Places) - Geofabrik GmbH](https://tools.geofabrik.de/osmi/views/places/wxs) - [GetCapabilities](https://tools.geofabrik.de/osmi/views/places/wxs?REQUEST=GetCapabilities&SERVICE=WMS)

* [Geofabrik Tools: OSM Inspector (Public Transport - Stops) - Geofabrik GmbH](https://tools.geofabrik.de/osmi/views/pubtrans_stops/wxs) - [GetCapabilities](https://tools.geofabrik.de/osmi/views/pubtrans_stops/wxs?REQUEST=GetCapabilities&SERVICE=WMS)

* [Geofabrik Tools: OSM Inspector (Public Transport Routes) - Geofabrik GmbH](https://tools.geofabrik.de/osmi/views/pubtrans_routes/wxs) - [GetCapabilities](https://tools.geofabrik.de/osmi/views/pubtrans_routes/wxs?REQUEST=GetCapabilities&SERVICE=WMS)

* [Geofabrik Tools: OSM Inspector (Routing) - Geofabrik GmbH](https://tools.geofabrik.de/osmi/views/routing/wxs) - [GetCapabilities](https://tools.geofabrik.de/osmi/views/routing/wxs?REQUEST=GetCapabilities&SERVICE=WMS)

* [Geofabrik Tools: OSM Inspector (Water2) - Geofabrik GmbH](https://tools.geofabrik.de/osmi/view/water/wxs) - [GetCapabilities](https://tools.geofabrik.de/osmi/view/water/wxs?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRIM_HR_FalseColour_2018 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/HRIM_HR_FalseColour_2018/ImageServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/HRIM_HR_FalseColour_2018/ImageServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRIM_HR_FalseColour_Cov1_2015 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/HRIM_HR_FalseColour_Cov1_2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/HRIM_HR_FalseColour_Cov1_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRIM_HR_FalseColour_Cov2_2015 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/HRIM_HR_FalseColour_Cov2_2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/HRIM_HR_FalseColour_Cov2_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRIM_HR_TrueColour_2018 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/HRIM_HR_TrueColour_2018/ImageServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/HRIM_HR_TrueColour_2018/ImageServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_DLT_2012_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_DLT_2012_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_DLT_2012_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_DLT_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_DLT_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_DLT_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_DominantLeafType_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_DominantLeafType_2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_DominantLeafType_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_FADSL_2012_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_FADSL_2012_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_FADSL_2012_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_FADSL_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_FADSL_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_FADSL_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ForestAdditionalSupportLayer_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ForestAdditionalSupportLayer_2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ForestAdditionalSupportLayer_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ForestAdditionalSupportLayer_2015 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ForestAdditionalSupportLayer_2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ForestAdditionalSupportLayer_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ForestType_2015 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ForestType_2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ForestType_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_Forest_Cover_Type_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_Forest_Cover_Type_2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_Forest_Cover_Type_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_FTY_2012_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_FTY_2012_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_FTY_2012_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_FTY_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_FTY_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_FTY_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_Grassland_2015 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_Grassland_2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_Grassland_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_GRAVPI_2015 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_GrasslandProbabilityIndex_2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_GrasslandProbabilityIndex_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_GRAVPI_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_GRAVPI_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_GRAVPI_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_GRA_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_GRA_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_GRA_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMCC_06_12_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMCC_06_12_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMCC_06_12_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMCC_2009_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMCC_2009_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMCC_2009_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMCC_2012_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMCC_2012_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMCC_2012_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMCC_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMCC_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMCC_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMC_06_12_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMC_06_12_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMC_06_12_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMC_2009_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMC_2009_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMC_2009_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMC_2012_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMC_2012_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMC_2012_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMC_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMC_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMC_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMD_2006_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMD_2006_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMD_2006_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMD_2009_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMD_2009_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMD_2009_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMD_2012_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMD_2012_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMD_2012_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMD_2012_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_TCD_2012_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_TCD_2012_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMD_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMD_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_IMD_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_IMD_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_TCD_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_TCD_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessChange_06_09 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessChange_06_09/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessChange_06_09/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessChange_06_12 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessChange_06_12/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessChange_06_12/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessChange_09_12 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessChange_09_12/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessChange_09_12/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessChange_12_15 - EEA ](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessChange_12_15/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessChange_12_15/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessClassifiedChange_06_09 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessClassifiedChange_06_09/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessClassifiedChange_06_09/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessClassifiedChange_06_12 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessClassifiedChange_06_12/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessClassifiedChange_06_12/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessClassifiedChange_09_12 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessClassifiedChange_09_12/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessClassifiedChange_09_12/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessClassifiedChange_12_15 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessClassifiedChange_12_15/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessClassifiedChange_12_15/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessDensity_2006 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessDensity_2006/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessDensity_2006/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessDensity_2006 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/SoilSealing/SoilSealingV1_Image_LAEA/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/SoilSealing/SoilSealingV1_Image_LAEA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessDensity_2006 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/SoilSealing/SoilSealingV2_Image_LAEA/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/SoilSealing/SoilSealingV2_Image_LAEA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessDensity_2009 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessDensity_2009/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessDensity_2009/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessDensity_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/GHSL_100m_LAEA/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/GHSL_100m_LAEA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessDensity_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_Imperviousness_Density_2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_Imperviousness_Density_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_ImperviousnessDensity_2015 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessDensity_2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_ImperviousnessDensity_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_PloughingIndicator_2015 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_PloughingIndicator_2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_PloughingIndicator_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_PLOUGH_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_PLOUGH_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_PLOUGH_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_SmallWoodyFeatures_2015_005m - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_SmallWoodyFeatures_2015_005m/ImageServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_SmallWoodyFeatures_2015_005m/ImageServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_STL_2012_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/UA_STL_2012_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/UA_STL_2012_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_SWF_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_SWF_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_SWF_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_TCDC_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_TCDC_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_TCDC_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_TreeCoverDensityChange_12_15 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_TreeCoverDensityChange_12_15/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_TreeCoverDensityChange_12_15/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_TreeCoverDensity_2015 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_TreeCoverDensity_2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_TreeCoverDensity_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_Tree_Cover_Density_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_Tree_Cover_Density_2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_Tree_Cover_Density_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_WaterWetnessProbabilityIndex_2015 - EEA ](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_WaterWetnessProbabilityIndex_2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_WaterWetnessProbabilityIndex_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_WaterWetness_2015 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_WaterWetness_2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_WaterWetness_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_WAW_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_WAW_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_WAW_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_Wetlands_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_Wetlands_2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/HRL_Wetlands_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [HRL_WWPI_2015_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_WWPI_2015_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/HRL_WWPI_2015_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LUCAS2015 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/LUCAS/LUCAS_2015/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/LUCAS/LUCAS_2015/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [LUCAS_2018 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/LUCAS/LUCAS_2018/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/LUCAS/LUCAS_2018/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [MapProxy WMS - maps.heigit.org](https://maps.heigit.org/histosm/wms) - [GetCapabilities](https://maps.heigit.org/histosm/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [MSC GeoMet — GeoMet-Weather 2.20.1 - Government of Canada, Environment and Climate Change Canada, Meteorological Service of Canada](https://geo.weather.gc.ca/geomet/) - [GetCapabilities](https://geo.weather.gc.ca/geomet/?REQUEST=GetCapabilities&SERVICE=WMS)

* [Natura2000_2006 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Natura2000/Natura2k_2006/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Natura2000/Natura2k_2006/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Natura2000_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Natura2000/Natura2k_2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Natura2000/Natura2k_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Noise Exposure - EEA](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/noise_exposure_2019/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/noise_exposure_2019/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Noise Sources 2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_Noise_Sources/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_Noise_Sources/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Noise Sources 2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_update2015_Noise_Sources/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_update2015_Noise_Sources/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [noiseStoryMap_NoiseContours_air_lnight - EEA](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/NoiseContours_air_lnight/ImageServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/NoiseContours_air_lnight/ImageServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [noiseStoryMap_NoiseContours_ind_lnight - EEA](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/NoiseContours_ind_lnight/ImageServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/NoiseContours_ind_lnight/ImageServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [noiseStoryMap_NoiseContours_rail_lden - EEA](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/NoiseContours_rail_lden/ImageServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/NoiseContours_rail_lden/ImageServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [noiseStoryMap_NoiseContours_road_lden - EEA](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/NoiseContours_road_lden/ImageServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/NoiseContours_road_lden/ImageServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [noiseStoryMap_NoiseContours_road_lnight - EEA](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/NoiseContours_road_lnight/ImageServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/NoiseContours_road_lnight/ImageServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [NOISE_END_LAEA.mxd - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_Major_Railways/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_Major_Railways/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [NOISE_END_LAEA.mxd - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_Major_Roads/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_Major_Roads/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [NOISE_END_LAEA.mxd - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_update2015_Major_Roads/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/2012_NOISE_END_LAEA_update2015_Major_Roads/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [OpenStreetMap (OSM) Demo WhereGroup - WhereGroup GmbH](https://osm-demo.wheregroup.com/service) - [GetCapabilities](https://osm-demo.wheregroup.com/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [OpenStreetMap WMS - terrestris GmbH und Co. KG](https://ows.terrestris.de/osm-gray/service) - [GetCapabilities](https://ows.terrestris.de/osm-gray/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [OpenStreetMap WMS - terrestris GmbH und Co. KG](https://ows.terrestris.de/osm/service) - [GetCapabilities](https://ows.terrestris.de/osm/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [OpenStreetMap WMS - terrestris GmbH und Co. KG](https://ows.mundialis.de/services/service) - [GetCapabilities](https://ows.mundialis.de/services/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [OSM-WMS HeiGIT - HeiGIT gGmbH](https://maps.heigit.org/osm-wms/service) - [GetCapabilities](https://maps.heigit.org/osm-wms/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [osmlanduse.org WMS - Heidelberg Universiy, Institute of Geography, Research Group of GIScience, HeiGIT](https://maps.heigit.org/osmlanduse/service) - [GetCapabilities](https://maps.heigit.org/osmlanduse/service?REQUEST=GetCapabilities&SERVICE=WMS)

* [RiperianZones_LCLU_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/RiparianZones/LCLU/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/RiparianZones/LCLU/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [RZ_LCLU_2012_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/RZ_LCLU_2012_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/RZ_LCLU_2012_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Sample Map - www2.demis.nl](https://www2.demis.nl/wms/wms.ashx) - [GetCapabilities](https://www2.demis.nl/wms/wms.ashx?REQUEST=GetCapabilities&SERVICE=WMS)

* [SeaDataNet Map Server - GHER, ULg, Belgium](https://sdn.oceanbrowser.net/web-vis/Python/web/wms) - [GetCapabilities](https://sdn.oceanbrowser.net/web-vis/Python/web/wms?REQUEST=GetCapabilities&SERVICE=WMS)

* [UA_BuildingHeight_2012_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/UA_BuildingHeight_2012_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/UA_BuildingHeight_2012_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [UA_StreetTreeLayer_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/UrbanAtlas/STL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/UrbanAtlas/STL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [UA_UrbanAtlasChange_2006_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/UrbanAtlas/UA_UrbanAtlasChange_2006_2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/UrbanAtlas/UA_UrbanAtlasChange_2006_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [UA_UrbanAtlasChange_2006_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/UrbanAtlas/UA_UrbanAtlas_2006/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/UrbanAtlas/UA_UrbanAtlas_2006/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [UA_UrbanAtlasChange_2006_2012_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/UA_UrbanAtlasChange_2006_2012_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/UA_UrbanAtlasChange_2006_2012_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [UA_UrbanAtlas_2012 - EEA](https://image.discomap.eea.europa.eu/arcgis/services/UrbanAtlas/UA_UrbanAtlas_2012/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/UrbanAtlas/UA_UrbanAtlas_2012/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [UA_UrbanAtlas_2012_DL - EEA](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/UA_UrbanAtlas_2012_DL/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/DownloadLayers/UA_UrbanAtlas_2012_DL/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Urban Atlas - EEA](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/Land_Use_Noise/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/Land_Use_Noise/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Urban Noise Grid - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/NoiseGrid_Dyna_LAEA/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/NoiseGrid_Dyna_LAEA/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [Urban Noise Grid - EEA](https://image.discomap.eea.europa.eu/arcgis/services/Noise/UrbanNoise_Dyna_WM/MapServer/WMSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/Noise/UrbanNoise_Dyna_WM/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - geo.limburg.be](https://geo.limburg.be/arcgis/services/SampleWorldCities/MapServer/WMSServer) - [GetCapabilities](https://geo.limburg.be/arcgis/services/SampleWorldCities/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS - sampleserver6.arcgisonline.com](https://sampleserver6.arcgisonline.com/arcgis/services/SampleWorldCities/MapServer/WMSServer) - [GetCapabilities](https://sampleserver6.arcgisonline.com/arcgis/services/SampleWorldCities/MapServer/WMSServer?REQUEST=GetCapabilities&SERVICE=WMS)

* [WMS TopPlusOpen - Dienstleistungszentrum des Bundes für Geoinformation und Geodäsie](https://sgx.geodatenzentrum.de/wms_topplus_open) - [GetCapabilities](https://sgx.geodatenzentrum.de/wms_topplus_open?REQUEST=GetCapabilities&SERVICE=WMS)

